import paho.mqtt.client as mqtt
import json
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import datetime as dt
import tkinter as tk
from tkinter import Label, filedialog
import glob
import numpy as np
import os
import matplotlib.dates as md
from decision_maker import Decision_Maker
from KalmanFilterForCSv4 import CS_Tracking
from time_functions import set_ts, get_ts


# * means all if need specific format then *.csv
# root = tk.Tk()
# root.withdraw()
# file_path = filedialog.askopenfilename()
# df = pd.read_csv(file_path)
# print(df)

# list_of_files = glob.glob('./data/all_data/*15*')
# list_of_files = glob.glob('./data/all_data/*15*')
# list_of_files = glob.glob('/home/albert/Downloads/23rd June Experiments/*.28*')
# list_of_files = glob.glob('/home/albert/Downloads/28th June Experiments-selected/*process*')
# list_of_files = glob.glob('./data/all_data/sigma*')
# list_of_files = glob.glob('/home/albert/Downloads/30th June Experiments-selected/*')
# list_of_files = glob.glob('/home/albert/Downloads/1st July Experiments-selected/*wifi*')
# list_of_files = glob.glob('/home/albert/Downloads/6st July Experiments-selected/**')
# latest_file = max(list_of_files, key=os.path.getctime)
# print(latest_file)
# df = pd.read_csv(latest_file)

all_data_files = []
all_data_files.append(glob.glob('/home/albert/Downloads/1st July Experiments-selected/*wifi*'))
all_data_files.append(glob.glob('/home/albert/Downloads/1st July Experiments-selected/*hotspot*'))
all_data_files.append(glob.glob('/home/albert/Downloads/1st July Experiments-selected/*5G*'))
# all_data_files.append(glob.glob('/home/albert/Downloads/6st July Experiments-selected/*'))

for file in all_data_files:
    df = pd.read_csv(file[0])

    # start_index = 0
    # end_index = 100
    
    # df = df.iloc[start_index:end_index]
    # df = df.iloc[0:2]

    ### Init decision object ###
    decision_maker = Decision_Maker()

    ### Filter out cs_data which is sent at lower than 30hz ###
    df_cs = df
    # df_cs = df_cs.drop(df_cs[df_cs.cs_y > 5].index)
    df_cs = df_cs.drop(df_cs[df_cs.cs_y == 0].index)
    # df_cs = df_cs.drop(df_cs[df_cs.cs_y == 0].index)
    df_cs['cs_diff'] = df_cs.cs_kf_measurement_ts.diff().iloc[1:]
    # print(df_cs.cs_diff.iloc[1:-1])
    df_cs = df_cs.drop(df_cs[df_cs.cs_diff == 0].index)
    df_cs = df_cs.iloc[1:]

    calibration_data = np.array(
    [[0, 88.0, -3.452583789988425, 3.832544301534088],
    [1, 29.0, -3.393933777729976, 2.3464297562596403],
    [2, 315.0, -3.89952740501977, 0.4261281050937278],
    [3, 379.0, -3.901613041341622, -1.209882992853374],
    [4, 413.0, -3.952087739375784, -2.420401650120553],
    [5, 442.0, -3.9481053763855054, -3.769397512819591],
    [6, 467.0, -3.973790035585145, -5.072811430776966],
    [7, 488.0, -3.995571195910231, -6.243005303495374],
    [8, 506.0, -3.999282785550153, -7.698859871276083],
    [9, 521.0, -4.016180564104874, -8.958135974467675],
    [10, 532.0, -3.9944304509557744, -9.91316514296864],
    [11, 543.0, -4.0258798498568105, -11.010880173316078],
    [12, 552.0, -4.02147911934151, -12.166513287789524],
    [13, 562.0, -4.000023199858463, -13.450424406101089],
    [14, 571.0, -3.977934988256585, -14.666712401267317],
    [15, 578.0, -4.027903397063103, -15.936420906607903]])

    decision_maker.calibrate(calibration_data)

    # ### Variables for decision making ###
    cv_max_speed = 2
    max_accel = 0.2
    sigma = 5
    safety_distance = 2
    time_horizon = 20
    plot_dist = 0
    risk = 0

    ########## Recalculate KF ##########
    ###  filter the distances using KF ###
    # Update the position array, obs only y values
    # max_nan_values = 30  # Should equal to 1 seconds
    # ts_arr = max_nan_values * [0]
    # px_arr = max_nan_values * [0]
    # py_arr = max_nan_values * [0]

    # kf_width = 10
    # kf_exists = False  # this is set true when we have received 10 values
    # # kf_cs = CS_Tracking(kf_width * [0.001], kf_width * [0.001], kf_width * [0.001])
    # kf_cs = CS_Tracking(-1,-1,-1)

    # def filter_nan(arr):
    #     mask = np.logical_or(arr == 0.0, arr > 100)
    #     return arr[mask]
    # df_cs['cs_kf_speed_new'] = df_cs['cs_y']

    # for index, row in df_cs.iterrows():
    #     ts_arr.pop()
    #     ts_arr.insert(0, row['cs_receive_ts'])
    #     px_arr.pop()
    #     px_arr.insert(0, row['cs_x'])
    #     py_arr.pop()
    #     py_arr.insert(0, row['cs_y'])
    #     # print(row['cs_x'],row['cs_y'])

    #     # print(str(row['cs_receive_ts'])+' ' + str(row['cs_y']))
    #     # check if we should reset the filter
    #     if kf_exists == True:
    #         if len(filter_nan(np.array(py_arr))) == max_nan_values:
    #             print(max_nan_values, "unreasonable in a row")
    #             print("Deleting KF instance at index", index)
    #             kf_ts = -1 #kf_width * [0]
    #             kf_px = -1 #kf_width * [0]
    #             kf_py = -1 #kf_width * [0]
    #             kf_cs = CS_Tracking(kf_ts, kf_px, kf_py)  # forgot this?
    #             kf_exists = False
    #         else:
    #             # kf_cs.new_meas(row['cs_receive_ts'], cs_x, cs_y)  # update values
    #             # print(row['cs_receive_ts'], row['cs_x'], row['cs_y'])
    #             kf_cs.new_meas(row['cs_receive_ts'],
    #                             row['cs_x'], row['cs_y'])

    #     # create a filter unless there exist one
    #     if kf_exists == False:
    #         # check that we have enough values to create the kf object
    #         if (len(filter_nan(np.array(py_arr[0:kf_width]))) == 0):
    #             print('initialized kf at index', index)
    #             print(ts_arr[0:kf_width], px_arr[0:kf_width], py_arr[0:kf_width])
    #             kf_cs = CS_Tracking(
    #                 ts_arr[0:kf_width], px_arr[0:kf_width], py_arr[0:kf_width])
    #             kf_exists = True

    #     if kf_exists:
    #         # update values, obs we only use time here
    #         # print("printing row")
    #         # print(row)
    #         kf_cs.new_meas(row['cs_kf_prediction_ts'], 0.0, 0.0)
    #         df_cs.loc[index,'cs_kf_x_new'] = float(kf_cs.data['posx'][0])
    #         df_cs.loc[index,'cs_kf_y_new'] = float(kf_cs.data['posy'][0])
    #         # print(float(kf_cs.data['posy'][0]))
    #         df_cs.loc[index,'cs_kf_vx_new'] = float(kf_cs.data['velx'][0])
    #         df_cs.loc[index,'cs_kf_vy_new'] = float(kf_cs.data['vely'][0])
            
    #         vx = float(kf_cs.data['velx'][0])
    #         vy = float(kf_cs.data['vely'][0])

    #         df_cs.loc[index,'cs_kf_posx_var_new'] = float(kf_cs.data['posx_var'][0])
    #         df_cs.loc[index,'cs_kf_posy_var_new'] = float(kf_cs.data['posy_var'][0])
    #         df_cs.loc[index,'cs_kf_velx_var_new'] = float(kf_cs.data['velx_var'][0])
    #         df_cs.loc[index,'cs_kf_vely_var_new'] = float(kf_cs.data['vely_var'][0])
    #         # df_cs.loc[index,'cs_kf_dt_new'] = float(kf_cs.data['dt'][0])
    #         df_cs.loc[index, 'cs_kf_speed_new'] = np.sqrt(
    #             vx**2 + vy**2)

    ### Calculate decision_maker
    # sigmas = [0,1,3,5]
    # for s in sigmas:        
    #     df['risk_tuned'+str(s)] = df.apply(lambda row: decision_maker.calculate_risk(row['cs_receive_ts'], row['decision_ts'], [row['cs_kf_x'], row['cs_kf_y']],
    #                                                                             [row['cs_kf_vx'],
    #                                                                                 row['cs_kf_vy']],
    #                                                                             [row['cs_kf_x_var'],
    #                                                                                 row['cs_kf_y_var']],
    #                                                                             [row['cs_kf_vx_var'],
    #                                                                                 row['cs_kf_vy_var']],
    #                                                                             [row['cv_self_x'],
    #                                                                             # row['cv_self_y']], [row['cv_self_vx'], row['cv_self_vy']],
    #                                                                             row['cv_self_y']], [0, 0],
    #                                                                             max_accel, s, safety_distance,
    #                                                                             plot_dist, time_horizon, cv_max_speed)[0], axis=1)
    df['send_rcv_diff'] = df['cs_y']
    for index, row in df.iterrows():
        df.loc[index, 'send_rcv_diff'] = row['cs_receive_ts'] - row['cs_send_ts']

    df['ros_delay'] = df['cs_y']
    for index, row in df.iterrows():
        df.loc[index, 'ros_delay'] = row['cv_receive_ts'] - row['cv_send_ts']

    # df['cv_latency_ma'] = df['cs_y']
    # df['cs_latency_ma5'] = df['cs_y'] # trash value
    # df['cs_latency_ma10'] = df['cs_y'] # trash value
    # df['cs_latency_ma20'] = df['cs_y'] # trash value
    # df['cs_latency_ma100'] = df['cs_y'] # trash value

    # def moving_average(x, w):
    #     return np.convolve(x, np.ones(w), 'valid') / w

    # ma_widths = [5, 10, 20, 100]
    # for w in ma_widths:
    #     l_cs = w * [0]

    #     for index, row in df.iterrows():
    #         l_cs.pop()
    #         l_cs.insert(0, row['cs_latency'])
    #         l_value = float(moving_average(l_cs, w))
    #         df.loc[index, 'cs_latency_ma'+str(w)] = l_value

    # # print(df['risk_tuned00'])
    #     # df['risk_tuned'] = df.apply(lambda row: row['risk_t'], axis=1)
    # df['cs_kf_speed'] = df.apply(lambda row: np.sqrt(row['cs_kf_vx']**2 + row['cs_kf_vy']**2), axis=1)

    # pd.DataFrame(df).to_csv('hi.csv', mode='a',header=(not os.path.exists("hi.csv")))

    # def get_last_y_go(df):
    #     for index, row in df.iterrows():
    #         if row['risk_generated'] == 1:
    #             return df['cs_kf_y'].loc[index]

    # def get_last_sec_go(df):
    #     for index, row in df.iterrows():
    #         if row['risk_generated'] == 1:
    #             return (index - start_index)/30

    # last_y = get_last_y_go(df)
    # last_go = get_last_sec_go(df)
    # print('last_y', last_y)

    # # df['avg_py'] = df.iloc[:, -1].rolling(window=10).mean()
    # df['avg_py'] = df['py'].rolling(window=10).mean()
    # df['avg_px'] = df['px'].rolling(window=10).mean()
    # df['10_time'] = (df['ts'][0].value / 10**9 - df['ts'][0].value / 10**9)
    # df['10_distance_x'] = df['avg_px']
    # df['10_distance_y'] = df['avg_py']
    # df['10_distance'] = df['avg_py']
    # # df['10_time'] = df.apply(lambda row: row['10_time'] / 10**9, axis=1)
    # for i in range(df.shape[0] - 10):
    #     df['10_distance_x'][i+10] = np.abs(df['px'][i+10] - df['px'][i])
    #     df['10_distance_y'][i+10] = np.abs(df['py'][i+10] - df['py'][i])
    #     df['10_distance'][i+10] = np.linalg.norm(np.array(df['px'][i+10], df['py'][i+10]) - np.array(df['px'][i], df['py'][i]))
    #     df['10_time'][i+10] = df['ts'][i+10].value / 10**9 - df['ts'][i].value / 10**9


    # df['avg_speed'] = df.apply(lambda row: np.divide(row['10_distance'], row['10_time']) , axis=1)

    # # df['avg_py'] = df.iloc[:, -1].rolling(window=10).mean()
    # # df['avg_speed']
    # print(df.iloc[500:600])
    # f = plt.figure()
    # ax = f.subplots(111)
    # fig, (ax1, ax2) = plt.subplots(2)

    # ax = plt.subplot(1, 1, 1)
    # # x_axis = np.arange(len(df))/1
    x_axis = np.arange(len(df))/30
    # dates = [dt.datetime.fromtimestamp(ts) for ts in df['decision_ts']]
    # # print(dates)
    # # print("decision ts:", get_ts(df['cs_receive_ts'])['ts_str'])
    # datenums = md.date2num(dates)
    # # exit()
    # ax = plt.gca()
    network = ''
    if 'wifi' in file[0]:
        network = 'wifi'
    elif 'hotspot' in file[0]:
        network = '4G hotspot'
    elif '5G' in file[0]:
        network = '5G'
    else:
        network = ''

    ### Risks ###
    # plt.plot(x_axis,df['risk_state'], label='risk_original_0', marker='.')
    # plt.plot(x_axis,df['risk_tuned0'], label='risk_sigma_0')
    # plt.plot(x_axis,df['risk_tuned1'], label='risk_sigma_1')
    # plt.plot(x_axis,df['risk_tuned3'], label='risk_sigma_3')
    # plt.plot(x_axis,df['risk_tuned5'], label='risk_sigma_5')

    ### Latencies ###
    # plt.plot(x_axis, df['cs_latency'], label='cs_latency')
    # plt.plot(x_axis, df['cs_latency_ma5'], label='cs_latency_ma5')
    # plt.plot(x_axis, df['cs_latency_ma10'], label='cs_latency_ma10')
    # plt.plot(x_axis, df['cs_latency_ma20'], label='cs_latency_ma20')
    # plt.plot(x_axis, df['cs_latency_ma100'], label='cs_latency_ma100')
    df.hist(column='cs_latency', bins=100)
    title_string = 'CS latency (' + str(network) + ')'
    plt.title(title_string, fontsize=20)
    plt.xlabel('Time (seconds)', fontsize=16)
    plt.ylabel('Count', fontsize=16)
    plt.xlim(0, 0.1)
    save_path = "./images/" + title_string + '.png'
    plt.savefig(save_path)

    df.hist(column='cv_latency', bins=100)
    title_string = 'CV latency (' + str(network) + ')'
    plt.title(title_string, fontsize=20)
    plt.xlabel('Time (seconds)', fontsize=16)
    plt.ylabel('Count', fontsize=16)
    plt.xlim(0, 0.1)
    save_path = "./images/" + title_string + '.png'
    plt.savefig(save_path)

    plt.figure()
    # plt.plot(x_axis, df['send_rcv_diff'], label='send_rcv_diff')
    plt.plot(x_axis, df['cs_latency'], label='cs_latency')
    plt.plot(x_axis, df['cv_latency'], label='cv_latency')
    # plt.plot(x_axis, df['ros_delay'], label='ros_delay')
    # print(np.mean(df['ros_delay']))

    ### Position velocities ###
    # plt.plot(x_axis, df['cs_y'], label='cs_y')
    # plt.plot(x_axis,df['cs_kf_y'], label='cs_kf_y')
    # plt.plot(x_axis,df['cv_kf_y'], label='cv_kf_y')
    # plt.plot(x_axis,df['cv_y'], label='cv_y')
    # plt.plot('cv_self_y'], label='cv_self_y')
    # plt.plot('cv_kf_speed'], label='cv_kf_speed')
    # plt.plot(x_axis, df['cs_kf_speed'], label='cs_kf_speed')
    # plt.plot(x_axis, df['cs_kf_speed'], label='cs_kf_speed')
    # plt.plot_date(x_axis, df_cs['cs_kf_speed_new'], label='cs_kf_speed_new')
    # plt.plot(x_axis, df['cs_kf_vy_var'], label='cs_kf_vy_var')
    # plt.plot(x_axis, df['cs_kf_y_var'], label='cs_kf_y_var')
    # plt.plot(df['cs_kf_speed_new'], label='cs_kf_speed_new')

    # plt.plot(df['cv_self_y'], label='cv_self_y')
    # plt.text(0.8, 0.7,
    #         'cv_max_speed = ' + str(cv_max_speed) + '\n'
    #         'max_accel = ' + str(max_accel) + '\n'
    #         #  'sigma = ' + str(sigma) + '\n'
    #         'safety_distance = ' + str(safety_distance) + '\n'
    #         'time_horizon = ' + str(time_horizon) + '\n',
    #         transform=ax.transAxes,
    #         fontsize='medium'
    #         )
    # plt.text(0.1, 0.7,
    #         'cv_max_speed = ' + str(cv_max_speed) + '\n'
    #         'max_accel = ' + str(max_accel) + '\n'
    #         #  'sigma = ' + str(sigma) + '\n'
    #         'safety_distance = ' + str(safety_distance) + '\n'
    #         'time_horizon = ' + str(time_horizon) + '\n',
    #         transform=ax.transAxes,
    #         fontsize='medium'
    #         )
    # plt.axhline(y=0.5, color='red', linestyle='--',
    #             linewidth=1, label='0.5 m/s')
    # plt.axhline(y=1, color='blue', linestyle='--',
    #             linewidth=1, label='1.0 m/s')
    # plt.axhline(y=1.5, color='orange', linestyle='--',
    #             linewidth=1, label='1.5 m/s')
    # plt.axhline(y=2.0, color='purple', linestyle='--',
    #             linewidth=1, label='2.0 m/s')

    ### check what file we have for labels ###

    print(network)
    plt.legend(fontsize=16, loc=2)
    plt.xlabel('Time (seconds)', fontsize=16)
    plt.ylabel('Time (seconds)', fontsize=16)
    title_string = 'Latency (' + str(network) +  ')'
    plt.title(title_string, fontsize=20)
    save_path = "./images/" + title_string
    plt.ylim(0, 0.2)

    plt.savefig(save_path)
    plt.figure()

plt.show()
